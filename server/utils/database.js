import mongoose from "mongoose";
import { config } from "dotenv";

config();

const { NODE_ENV, DEV_DB } = process.env;

const options = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
};

mongoose
    .connect(DEV_DB, options)
    .then(() => {
        console.log("database successfully connected");
    })
    .catch((err) => {
        console.log("database connection failed");
        console.log(err);
    });
