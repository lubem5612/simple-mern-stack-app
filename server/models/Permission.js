import mongoose from 'mongoose';

let PermissionSchema = new mongoose.Schema({
    name: {type: String, unique: true, required: true},
    users: Array,
    created_at: { type: Date, default: Date.now },
    updated_at: {type: Date},
});

export default mongoose.model('Permission', PermissionSchema);
