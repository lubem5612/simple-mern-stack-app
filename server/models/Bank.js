import mongoose from 'mongoose';

let BankSchema = new mongoose.Schema({
    name: {type: String, unique: true, required: true},
    code: String,
    created_at: { type: Date, default: Date.now },
    updated_at: {type: Date},
});

export default mongoose.model('Bank', BankSchema);
