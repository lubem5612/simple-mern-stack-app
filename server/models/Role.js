import mongoose from 'mongoose';

let RoleSchema = new mongoose.Schema({
    name: {type: String, unique: true, required: true},
    company_id: String,
    users: Array,
    created_at: { type: Date, default: Date.now },
    updated_at: {type: Date},
});

export default mongoose.model('Department', RoleSchema);
