import mongoose from 'mongoose';

let InstitutionSchema = new mongoose.Schema({
    name: {type: String, unique: true, required: true},
    company_id: String,
    created_at: { type: Date, default: Date.now },
    updated_at: {type: Date},
});

export default mongoose.model('Institution', InstitutionSchema);
