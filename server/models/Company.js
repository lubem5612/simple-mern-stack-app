import mongoose from 'mongoose';

let CompanySchema = new mongoose.Schema({
    user_id: mongoose.Schema.Types.ObjectId,
    name: {type: String, unique: true, required: true},
    rc_number: String,
    phone: String,
    description: String,
    address: String,
    motto: String,
    vision: String,
    mission: String,
    established_date: {type: Date},
    category: String,
    approval_status: {type:Boolean, default: false},
    logo_url: String,
    publisher: String,
    created_at: { type: Date, default: Date.now },
    updated_at: {type: Date},
});

export default mongoose.model('Company', CompanySchema);
