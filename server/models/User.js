import mongoose, {Schema} from 'mongoose';
import jwt from "jsonwebtoken";
import bcrypt from 'bcrypt-nodejs';
import {config} from "dotenv";

config();

const UserSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: true,
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
    },
    verified: {
        type: Boolean,
        default: false,
    },
    name: {
        type: String,
        required: true
    },
    company: Object,
    status: {
        type: Number,
        default: 1,
    },
    category: {
       type: String,
       default: 'admin',
    },
    token: {
        type: String,
    },
    staff: {
        phone: String,
        gender: {type: Boolean, default: true},
        salary: Number,
        address: String,
        photo_url: String,
        father: String,
        mother: String,
        next_of_kin: String,
        next_of_kin_address: String,
        account_details: {
            bank: {
                name: String,
                code: String,
            },
            number: Number,
            name: String,
        },
        promotion: {
            first: Date,
            last: Date,
            next_due: Date,
            next_actual: Date,
        },
        appointment_date: Date,
        retirement_date: Date,
        level: Object,
        job_title: Object,
        position: Object,
        educational_qualification: Object,
        credentials: {
            name: String,
            file_url: String,
            extension: String,
            size: Number,
        }
    },
    roles: [{
        name: String
    }],
    permissions: [{
        name: String
    }],
    created_at: {
        type: Date,
        default: Date.now(),
    },
    updated_at: {
        type: Date,
    },
});

UserSchema.pre('save', function (next) {
    let user = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(process.env.HASH, function (err, salt) {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.password, salt, null, function (err, hash) {
                if (err) {
                    return next(err);
                }
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});

UserSchema.methods.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};

UserSchema.methods.generateToken = function (cb) {
    let user = this;
    let token = jwt.sign(user._id.toHexString(), process.env.PRIVATE_KEY);
    user.token = token;
    user.save(function (err, user) {
        if(err)
            return cb(err);
        cb(null, user);
    })
};

UserSchema.statics.findByToken = function (token, cb) {
    let user = this;
    jwt.verify(token, process.env.PRIVATE_KEY, function (err, decode) {
        user.findOne({"_id": decode, "token": token}, function (err, user) {
            if(err)
                return cb(err);
            cb(null, user);
        })

    })
};

UserSchema.methods.deleteToken = function (token, cb) {
    let user = this;
    user.updateOne({$unset : {token: 1}}, function (err, user) {
        if(err)
            return cb(err);
        cb(null, user)
    })
};

export default mongoose.model('User', UserSchema);
