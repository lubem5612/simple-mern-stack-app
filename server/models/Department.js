import mongoose from 'mongoose';

let DepartmentSchema = new mongoose.Schema({
    name: {type: String, unique: true, required: true},
    company_id: String,
    head_of_dept: Object,
    created_at: { type: Date, default: Date.now },
    updated_at: {type: Date},
});

export default mongoose.model('Department', DepartmentSchema);
