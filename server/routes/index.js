import express from "express";
import publicRoutes from "./unprotected";
import privateRoutes from "./protected";

const routes = express.Router();
routes.use('/', publicRoutes);
routes.use('/', privateRoutes);

export default routes;