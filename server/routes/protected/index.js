import express from "express";
import  companyRoute from "./companies.route";
import {auth} from "../../middlewares/auth";

const privateRoutes = express.Router();

//register routes here
privateRoutes.use('/admin/companies', auth, companyRoute);

export default privateRoutes;