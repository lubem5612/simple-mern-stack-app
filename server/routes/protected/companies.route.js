import express from "express";
import companyController from "../../controllers/admin/company.controller"

const route = express.Router();

route.get('/', companyController.index);
route.get('/:id', companyController.show);
route.put('/:id', companyController.update);
route.delete('/:id', companyController.destroy);

export default route;