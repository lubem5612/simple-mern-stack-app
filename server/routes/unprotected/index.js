import express from "express";
import  authRoute from "./auth.route";

const publicRoutes = express.Router();

//register routes here
publicRoutes.use('/auth', authRoute);

export default publicRoutes;
