import express from "express";
import {auth} from "../../middlewares/auth";
import authController from "../../controllers/auth/auth.controller";

const route = express.Router();

route.post('/login', authController.login);
route.post('/register', authController.register);
route.get('/user', auth, authController.loggedUser);
route.get('/logout', auth, authController.logout);

export default route;
