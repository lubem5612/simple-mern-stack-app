import Company from "../../models/Company";
import _ from "lodash";

export default {
    //routes GET /api/admin/companies, if query string, routes GET /api/admin/companies?name=raadaa&status=1
    index: (req, res) => {
        let noQuery = _.isEmpty(req.query);
        console.log(req.query, noQuery);
        if(noQuery) {
            Company.find().sort({created_at: -1}).exec(function (err, result) {
                if(err)
                    res.send(err);
                return Response.sendSuccess(res, result, 'companies obtained successfully');
            })
        }else {
            Company.find(req.query).sort({created_at: -1}).exec(function (err, result) {
                if(err)
                    res.send(err);
                return Response.sendSuccess(res, result, 'companies obtained successfully');
            })
        }
    },
    //routes GET /api/admin/companies/:id
    show: (req, res) => {
        Company.findById(req.params.id, function (error, company) {
            if(error)
                res.send(error);
            return Response.sendSuccess(res, company, 'company retrieved successfully');
        })
    },
    //routes PUT /api/admin/companies/:id
    update: (req, res) => {
        Company.findByIdAndUpdate(req.params.id, req.body, function (err, comp) {
            if(err)
                res.send(err);
            return Response.sendSuccess(res, comp, 'company updated successfully');

        })
    },
    //routes DELETE /api/admin/companies/:id
    destroy: (req, res) => {
        Company.findByIdAndRemove(req.params.id, function (err, comp) {
            if(err)
                res.send(err);
            return Response.sendSuccess(res, comp, 'company deleted successfully');
        })
    },

}