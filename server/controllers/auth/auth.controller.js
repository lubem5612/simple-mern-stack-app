import User from "../../models/User";
import {config} from "dotenv";
import Company from "../../models/Company";

config();

export default {
    login: (req,res) => {
        let token=req.cookies.auth;
        let {username, password} = req.body;

        User.findByToken(token,(err,user)=>{
            if(err) return  res(err);
            if(user){
                return Response.sendSuccess(res, {isLoggedIn: true}, 'you are already logged in');
            }
            else{
                User.findOne({username: username},function(err,user){
                    if(!user) return  Response.sendError(res, 'user does not exist for username');

                    user.comparePassword(password,(err,isMatch)=>{
                        if(!isMatch) return Response.sendError(res, "password doesn't match");

                        user.generateToken((err,user)=>{
                            if(err) return res.status(400).send(err);
                            res.cookie('auth',user.token).json({
                                isAuth : true,
                                id : user._id,
                                email : user.username
                            });
                        });
                    });
                });
            }
        });
    },

    register: (req, res) => {
        let {username, name, password, password_confirmation, company} = req.body;
        if(password !== password_confirmation) {
            return Response.sendError(res, 'Password confirmation failed, mismatched passwords');
        }
        User.findOne({
            username: username
        }, function (err, result) {
            if(err) throw err;
            if(result) {
                return Response.sendError(res, 'registration failed. user already exist');
            }
            let newUser = new User({
                name: name,
                username: username,
                password: password,
            });
            newUser.save(function (err, user) {
                if(err) throw err;
                let newCompany = new Company({
                    name: company,
                    user_id: user._id
                });
                newCompany.save(function (err, comp) {
                    if(err) throw err;
                    user.company = comp;
                    user.password = null;
                    return Response.sendSuccess(res, user, 'user registration successful');
                })
            })
        })
    },

    loggedUser: (req, res) => {
        if(!req.user){
            return Response.sendError(res, 'authenticated user not found');
        }
        return Response.sendSuccess(res, req.user, 'logged user retrieved successful');
    },

    logout: (req, res) => {
        req.user.deleteToken(req.token, (err, user) => {
            if(err) {
                return res.status(400).send(err);
            }
        });
        return Response.sendSuccess(res, {url: '/'}, 'user logged out successfully');
    }

}

