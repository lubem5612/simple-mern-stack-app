import express from "express";
import path from "path";
import logger from "morgan";
import bodyParser from "body-parser";
import morgan from "morgan";
import cors from "cors";
import {auth} from "./server/middlewares/auth";
import cookieParser from "cookie-parser";
import response from "./server/utils/response";
import mongoose from "mongoose";
import './server/utils/database';
import routes from "./server/routes";
import { config } from "dotenv";

const app = express();

config();

const {PORT} = process.env;

//global variables
global.Response = response;

//database connection
mongoose.Promise = global.Promise;

//middleware

app.use(logger('dev'));
app.use(cors());
app.use(cookieParser());
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended': 'false'}));
app.use(express.static(path.join(__dirname, 'build')));

app.get('/', (req, res) => res.json({
    status: 'Ok',
    message: 'welcome to bom application',
    data: null,
}));
app.get('/test', auth, function (req, res) {
    return res.json({data: 'welcome to test route'})
})
app.use('/api', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

//start server
app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});
